public class Exercicio19 {
   public static void executar(){

       Double raio = Prompt.lerDecimal("digite o raio do cilindro: ");
       Double altura = Prompt.lerDecimal("digite a altura do cilindro: ");

       Double volume = 3.14 *(raio * raio)*altura;

       Prompt.imprimir("o volume do cilindro e: " + volume);

   }
}
