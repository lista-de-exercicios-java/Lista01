public class Exercicio04 {
     
    public static void executar(){

        Double num1 = Prompt.lerDecimal("digite seu 1 numero: ");
        Double num2 = Prompt.lerDecimal("digite seu 2 numero: ");

        Double soma = num1 + num2;
        Double subtrair = num1 - num2;
        Double multiplicacao = num1 * num2;
        Double divisao = num1 / num2;

        Prompt.imprimir("a soma dos numeros e: " + soma);
        Prompt.imprimir("a subtracao dos numeros e: " + subtrair);
        Prompt.imprimir("a multiplicacao dos numeros e: " + multiplicacao);
        Prompt.imprimir("a divisao dos numeros e: " + divisao);
    }
}
