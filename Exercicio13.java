public class Exercicio13 {
    public static void executar(){

        Double num1 = Prompt.lerDecimal("digite seu 1 numero:");
        Double num2 = Prompt.lerDecimal("digite seu 2 numero:");

        char operador = Prompt.lerCaractere("digite o operador que voce quer usar ( a(+)  b(-)  c(*)  d(/)): ");

        switch (operador) {
            case 'a':

                 Double soma = num1 + num2;
                 Prompt.imprimir("a soma dos nomeros e: " + soma);
                
                break;

            case 'b':
                Double subtracao = num1 - num2;
                Prompt.imprimir("a subtracao dos numeros e: " + subtracao);
               
               break;

            case 'c':
               Double multiplicacao = num1 * num2;
               Prompt.imprimir("a multiplicacao dos numeros e : " + multiplicacao);
              
              break;

            case 'd':

              if(num2 != 0){
                Double divisao = num1 / num2;
                Prompt.imprimir("a divisao dos numeros e: " + divisao);
              }
              else{
                Prompt.imprimir("divisao por 0 ... digite outro numero");
              }
             
             break;

            default:
            Prompt.imprimir("operador invalido ");

                break;
       }
    }

}
